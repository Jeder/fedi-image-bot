from mastodon import Mastodon

mastodon = Mastodon(
    api_base_url="https://instance.name", client_id="mastodon.app.credentials"
)


### For Mastodon
# scopes = ["write"]
# print(mastodon.auth_request_url(scopes=scopes))
# mastodon.log_in(
#     to_file="mastodon.client.credentials", code=input("code: "), scopes=scopes
# )


### For Pleroma
mastodon.log_in(
    'my_login_email@example.com',
    'incrediblygoodpassword',
    to_file = 'mastodon.client.credentials'
)