import os
import pathlib
import random
import sys
from glob import glob
from html import unescape
from time import sleep

from mastodon import Mastodon, MastodonInternalServerError, MastodonBadGatewayError
from pybooru import Danbooru
from pybooru.exceptions import PybooruHTTPError
from saucenaopie import SauceNao
from saucenaopie.exceptions import ImageInvalid, LimitReached, SauceNaoError
from saucenaopie.helper import SauceIndex
from saucenaopie.types.sauce import BooruSauce, TwitterSauce

from danboorucred import apikey, saucenao_key, username

mastodon = Mastodon(
    access_token="mastodon.client.credentials",
    api_base_url="https://fedi.absturztau.be",
)


def dir_init():
    imdir = 'images/'
    if not os.path.isdir(imdir):
        os.makedirs(imdir)
        raise Exception(
            "Image folder doesn't have any images inside! Please add some")
    ext = ['png', 'jpg', 'gif']
    images = [img for e in ext for img in glob(f"{imdir}*.{e}")]
    if not images:
        raise Exception(
            "Image folder doesn't have any images inside! Please add some")
    return images


def cw_check(file):
    file = pathlib.Path(file).stem.split("_")
    dict = {
        'cw': False,
    }
    if len(file) > 1:
        match file[1]:
            case "nsfw":
                dict['cw'] = True
                dict['text'] = "possibly nsfw"
                return dict
            case "yuri":
                dict['cw'] = True
                dict['text'] = "yuri"
                return dict
            case _:
                return dict
    else:
        return dict


def get_source(file_path):
    file = pathlib.Path(file_path).stem.split("_")[0]
    # danbooru = Danbooru('danbooru') # feel free to not use auth though, i
    # don't even know what happened
    danbooru = Danbooru('danbooru', username=username, api_key=apikey)
    try:
        source = danbooru.post_show(file)['source']
        print(danbooru.last_call['status_code'])
    except PybooruHTTPError:
        print("Error trying to get source")
        if danbooru.last_call['status_code'] in (
                403,
                429,
                500,
                502,
                503):  # i have no fucking clue how to extract status_code from PybooruHTTPError itself LMAO
            # saucenao as fallback, hopefully not a one that will be used
            # always, because of 200(or apparently 100) requests per 24 hours
            # limit
            print(danbooru.last_call['status_code'] + "Trying saucenao")
            return saucenao(file_path)
        elif danbooru.last_call['status_code'] == 404:  # this shouldn't happen
            return None
        sleep(10)
        print("Trying again")
        source = danbooru.post_show(file)['source']

    source_list = source.split("/")
    if source_list[3] == 'img-original':
        return f'https://www.pixiv.net/member_illust.php?mode=medium&illust_id={source_list[-1].split("_")[0]}'
    return source


def saucenao(file_path):
    client = SauceNao(api_key=saucenao_key)
    try:
        source = client.search(file_path)
    except LimitReached as ex:
        print(f"Daily requests left: {ex.long_remaining}.")
        print(f"30 second requests left: {ex.short_remaining}.")
    except ImageInvalid:
        return "can't find source"
    except SauceNaoError:
        return "can't find source"
    client.close()
    results = []
    for result in source.get_likely_results():
        if result.index.id == SauceIndex.PIXIV:
            results.append(result.data.first_url)
        elif isinstance(result.data, BooruSauce):
            source_list = result.data.source_url.split("/")
            if source_list[3] == 'img-original':
                rs = f'https://www.pixiv.net/member_illust.php?mode=medium&illust_id={source_list[-1].split("_")[0]}'
            else:
                rs = result.data.source_url
            results.append(rs)
        elif isinstance(result.data, TwitterSauce):
            if result.data.username in (
                "AceYuriBot",
                "AceCatgirlBot",
                "AcePictureBot"):  # thanks acepicturebot for polluting results, very cool
                pass
            else:
                results.append(result.data.first_url)
    results = list(dict.fromkeys(results))
    return "\n".join(results)


if __name__ == '__main__':
    assert sys.version_info >= (3, 10)
    file = random.choice(dir_init())
    cw = cw_check(file)
    toot = None
    if cw['cw']:
        for attempt in range(10):
            try:
                toot = mastodon.status_post(f"source: {get_source(file)}",
                                            media_ids=mastodon.media_post(file),
                                            sensitive=True,
                                            spoiler_text={cw["text"]})
                break
            except MastodonInternalServerError:
                print("500 Interval Server Error\n\nRetrying")
                retry_time = 10 * int(attempt)
                sleep(retry_time)
            except MastodonBadGatewayError:
                print("502 Bad Gateway\n\nRetrying")
                retry_time = 10 * int(attempt)
                sleep(retry_time)
    else:
        for attempt in range(10):
            try:
                toot = mastodon.status_post(f"source: {get_source(file)}",
                                            media_ids=mastodon.media_post(file))
                break
            except MastodonInternalServerError:
                print("500 Interval Server Error\n\nRetrying")
                retry_time = 10 * int(attempt)
                sleep(retry_time)
            except MastodonBadGatewayError:
                print("502 Bad Gateway\n\nRetrying")
                retry_time = 10 * int(attempt)
                sleep(retry_time)

    print("Done\n")
    print(file)
    print("\n" + unescape(toot['content']))
